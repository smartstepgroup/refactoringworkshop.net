#region Usings

using System;
using VideoStore;

#endregion

namespace Tests {
    public abstract class Test {
        protected Test() {
            Create = new Father();
        }

        protected Father Create { get; private set; }

        protected class Father {
            public RegularMovie RegularMovie(string title = null) {
                title = title ?? UniqueString;
                return new RegularMovie(title);
            }

            public ChildrenMovie ChildrenMovie(string title = null) {
                title = title ?? UniqueString;
                return new ChildrenMovie(title);
            }

            public NewReleaseMovie NewReleaseMovie(string title = null) {
                title = title ?? UniqueString;
                return new NewReleaseMovie(title);
            }

            public Rental Rental(Movie movie = null, int daysRented = 1) {
                movie = movie ?? RegularMovie();
                return new Rental(movie, daysRented);
            }

            public CustomerBuilder Customer(string name = null) {
                name = name ?? UniqueString;
                return new CustomerBuilder(name, this);
            }

            private string UniqueString {
                get { return Guid.NewGuid().ToString(); }
            }

            public class CustomerBuilder {
                private readonly Father create;
                private readonly Customer customer;

                public CustomerBuilder(string name, Father create) {
                    this.create = create;
                    customer = new Customer(name);
                }

                public static implicit operator Customer(CustomerBuilder builder) {
                    return builder.customer;
                }

                public CustomerBuilder RentRegularMovie(int daysRented, string title) {
                    return Rental(daysRented, create.RegularMovie(title: title));
                }

                public CustomerBuilder RentChildrenMovie(int daysRented, string title) {
                    return Rental(daysRented, create.ChildrenMovie(title: title));
                }

                public CustomerBuilder RentNewReleaseMovie(int daysRented, string title) {
                    return Rental(daysRented, create.NewReleaseMovie(title: title));
                }

                private CustomerBuilder Rental(int daysRented, Movie movie) {
                    var rental = create.Rental(movie: movie, daysRented: daysRented);
                    customer.Add(rental);
                    return this;
                }
            }
        }
    }
}