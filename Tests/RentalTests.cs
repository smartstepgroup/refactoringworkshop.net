#region Usings

using NUnit.Framework;

#endregion

namespace Tests {
    [TestFixture]
    public class RentalTests : Test {
        [Test]
        public void GetDaysRented() {
            var rental = Create.Rental(daysRented: 1);
            Assert.AreEqual(1, rental.DaysRented);
        }

        [Test]
        public void GetMovie() {
            var movie = Create.RegularMovie();
            var rental = Create.Rental(movie: movie);
            Assert.AreEqual(movie, rental.Movie);
        }
    }
}