#region Usings

using NUnit.Framework;
using VideoStore;

#endregion

namespace Tests {
    [TestFixture]
    public class MovieTests : Test {
        [Test]
        public void GetPriceCode() {
            var movie = Create.RegularMovie();
            Assert.IsInstanceOf<RegularMovie>(movie);
        }

        [Test]
        public void SetPriceCode() {
            var movie = Create.ChildrenMovie();
            Assert.IsInstanceOf<ChildrenMovie>(movie);
        }

        [Test]
        public void GetTitle() {
            var movie = Create.RegularMovie(title: "Star Wars");
            Assert.AreEqual("Star Wars", movie.Title);
        }
    }
}