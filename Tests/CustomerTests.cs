#region Usings

using NUnit.Framework;
using VideoStore;

#endregion

namespace Tests {
    [TestFixture]
    public class CustomerTests : Test {
        [Test]
        public void GetName() {
            Customer customer = Create.Customer(name: "Luke Skywalker");
            Assert.AreEqual("Luke Skywalker", customer.Name);
        }

        [Test]
        public void StatementNoMovies() {
            Customer customer = Create.Customer(name: "Luke Skywalker");
            StringAssert.StartsWith("Rental Record for Luke Skywalker", customer.Statement());
        }

        [Test]
        public void StatementOneNewMovieOneDay() {
            Customer customer = Create.Customer()
                .RentNewReleaseMovie(daysRented: 1, title: "Star Wars III");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars III\t3\n", statement);
            StringAssert.EndsWith("Amount owed is 3\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneNewMovieTwoDay() {
            Customer customer = Create.Customer()
                .RentNewReleaseMovie(2, "Star Wars Episode I");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars Episode I\t6\n", statement);
            StringAssert.EndsWith("Amount owed is 6\nYou earned 2 frequent renter points", statement);
        }

        [Test]
        public void StatementOneNewMovieThreeDays() {
            Customer customer = Create.Customer()
                .RentNewReleaseMovie(3, "Star Wars VI");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars VI\t9\n", statement);
            StringAssert.EndsWith("Amount owed is 9\nYou earned 2 frequent renter points", statement);
        }

        [Test]
        public void StatementOneRegularMovieOneDay() {
            Customer customer = Create.Customer()
                .RentRegularMovie(1, "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars II\t2\n", statement);
            StringAssert.EndsWith("Amount owed is 2\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneRegularMovieTwoDay() {
            Customer customer = Create.Customer()
                .RentRegularMovie(2, "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars II\t2\n", statement);
            StringAssert.EndsWith("Amount owed is 2\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneRegularMovieThreeDays() {
            Customer customer = Create.Customer()
                .RentRegularMovie(3, "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars II\t3.5\n", statement);
            StringAssert.EndsWith("Amount owed is 3.5\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneChildMovieOneDay() {
            Customer customer = Create.Customer()
                .RentChildrenMovie(1, "Star Wars for Kids");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars for Kids\t1.5\n", statement);
            StringAssert.EndsWith("Amount owed is 1.5\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneChildMovieThreeDay() {
            Customer customer = Create.Customer()
                .RentChildrenMovie(3, "Star Wars for Kids");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars for Kids\t1.5\n", statement);
            StringAssert.EndsWith("Amount owed is 1.5\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementOneChildMovieFourDays() {
            Customer customer = Create.Customer()
                .RentChildrenMovie(daysRented: 4, title: "Star Wars for Kids");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars for Kids\t3\n", statement);
            StringAssert.EndsWith("Amount owed is 3\nYou earned 1 frequent renter points", statement);
        }

        [Test]
        public void StatementTwoNewMoviesThreeDays() {
            Customer customer = Create.Customer()
                .RentNewReleaseMovie(daysRented: 3, title: "Star Wars I")
                .RentNewReleaseMovie(daysRented: 3, title: "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars I\t9\n", statement);
            StringAssert.Contains("\tStar Wars II\t9\n", statement);
            StringAssert.EndsWith("Amount owed is 18\nYou earned 4 frequent renter points", statement);
        }

        [Test]
        public void StatementTwoRegularMoviesThreeDays() {
            Customer customer = Create.Customer()
                .RentRegularMovie(daysRented: 3, title: "Star Wars I")
                .RentRegularMovie(daysRented: 3, title: "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars I\t3.5\n", statement);
            StringAssert.Contains("\tStar Wars II\t3.5\n", statement);
            StringAssert.EndsWith("Amount owed is 7\nYou earned 2 frequent renter points", statement);
        }

        [Test]
        public void StatementTwoChildMoviesFourDays() {
            Customer customer = Create.Customer()
                .RentChildrenMovie(daysRented: 4, title: "Star Wars I")
                .RentChildrenMovie(daysRented: 4, title: "Star Wars II");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars I\t3\n", statement);
            StringAssert.Contains("\tStar Wars II\t3\n", statement);
            StringAssert.EndsWith("Amount owed is 6\nYou earned 2 frequent renter points", statement);
        }

        [Test]
        public void StatementAllThreeTypesMoviesFourDays() {
            Customer customer = Create.Customer()
                .RentNewReleaseMovie(daysRented: 4, title: "Star Wars I")
                .RentRegularMovie(daysRented: 4, title: "Star Wars II")
                .RentChildrenMovie(daysRented: 4, title: "Star Wars III");

            var statement = customer.Statement();

            StringAssert.Contains("\tStar Wars I\t12\n", statement);
            StringAssert.Contains("\tStar Wars II\t5\n", statement);
            StringAssert.Contains("\tStar Wars III\t3\n", statement);
            StringAssert.EndsWith("Amount owed is 20\nYou earned 4 frequent renter points", statement);
        }
    }
}