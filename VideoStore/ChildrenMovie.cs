namespace VideoStore {
    public class ChildrenMovie : Movie {
        public ChildrenMovie(string title) : base(title) {}

        public override double Amount(int daysRented) {
            var amount = 1.5;

            if (daysRented > 3) {
                amount += ((daysRented - 3)*1.5);
            }
            return amount;
        }
    }
}