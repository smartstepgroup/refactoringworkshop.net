#region Usings

using System.Collections.Generic;
using System.Linq;

#endregion

namespace VideoStore {
    public class Customer {
        private readonly string name;
        private readonly List<Rental> rentals = new List<Rental>();

        public Customer(string name) {
            this.name = name;
        }

        public string Name {
            get { return name; }
        }

        public void Add(Rental rental) {
            rentals.Add(rental);
        }

        public string Statement() {
            return StatementHeader() +
                   StatementBody() +
                   StatementFooter();
        }

        private string StatementBody() {
            var result = "";
            foreach (var rental in rentals) {
                result += StatementBodyLine(rental);
            }
            return result;
        }

        private static string StatementBodyLine(Rental rental) {
            return "\t" + rental.Movie.Title + "\t" + rental.Amount + "\n";
        }

        private double CalculateTotalAmount() {
            return rentals.Sum(rental => rental.Amount);
        }

        private int CalculateTotalFrequentRentalPoints() {
            return rentals.Sum(rental => rental.FrequentRentalPoints);
        }

        private string StatementFooter() {
            return
                FooterWithTotalAmount(CalculateTotalAmount()) +
                FooterWithTotalFrequentRentalPoints(CalculateTotalFrequentRentalPoints());
        }

        private string StatementHeader() {
            return "Rental Record for " + Name + "\n";
        }

        private static string FooterWithTotalFrequentRentalPoints(int totalFrequentRenterPoints) {
            return ("You earned " + totalFrequentRenterPoints + " frequent renter points");
        }

        private static string FooterWithTotalAmount(double totalAmount) {
            return ("Amount owed is " + totalAmount + "\n");
        }
    }
}