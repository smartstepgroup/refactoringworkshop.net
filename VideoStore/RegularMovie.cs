namespace VideoStore {
    public class RegularMovie : Movie {
        public RegularMovie(string title) : base(title) {}

        public override double Amount(int daysRented) {
            double regularAmount = 2;

            if (daysRented > 2) {
                regularAmount += ((daysRented - 2)*1.5);
            }

            return regularAmount;
        }
    }
}