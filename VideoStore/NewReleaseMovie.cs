namespace VideoStore {
    public class NewReleaseMovie : Movie {
        public NewReleaseMovie(string title) : base(title) {}

        public override double Amount(int daysRented) {
            return daysRented*3;
        }

        public override int GetFrequentRentalPoints(int daysRented) {
            return daysRented > 1
                ? 2
                : base.GetFrequentRentalPoints(daysRented);
        }
    }
}