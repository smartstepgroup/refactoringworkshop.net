#region Usings

using System;

#endregion

namespace VideoStore {
    public abstract class Movie {
        protected Movie(string title) {
            Title = title;
        }

        public int PriceCode { get; private set; }

        public String Title { get; private set; }
        public abstract double Amount(int daysRented);

        public virtual int GetFrequentRentalPoints(int daysRented) {
            return 1;
        }
    }
}