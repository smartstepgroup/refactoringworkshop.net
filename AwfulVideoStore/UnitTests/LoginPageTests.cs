﻿#region Usings

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace UnitTests {
    [TestClass]
    public class LoginPageTests {
        [TestMethod]
        public void AdminCanLogin() {
            var loginPage = new TestableLogin();
            loginPage.EnteredUserName = "admin";
            loginPage.EnteredPassword = "admin";

            loginPage.LoginButtonClick(null, null);

            Assert.AreEqual("admin", loginPage.SavedLogin);
        }

        [TestMethod]
        public void AdminWithWrongPasswordCanNotLogin() {
            var loginPage = new TestableLogin();
            loginPage.EnteredUserName = "admin";
            loginPage.EnteredPassword = "WRONG";

            loginPage.LoginButtonClick(null, null);

            Assert.IsNull(loginPage.SavedLogin);
        }

        [TestMethod]
        public void UserCanLogin() {
            var loginPage = new TestableLogin();
            loginPage.EnteredUserName = "user";
            loginPage.EnteredPassword = "user";

            loginPage.LoginButtonClick(null, null);

            Assert.AreEqual("user", loginPage.SavedLogin);
        }

        [TestMethod]
        public void UserWithWrongPasswordCanNotLogin() {
            var loginPage = new TestableLogin();
            loginPage.EnteredUserName = "user";
            loginPage.EnteredPassword = "WRONG";

            loginPage.LoginButtonClick(null, null);

            Assert.IsNull(loginPage.SavedLogin);
        }

        [TestMethod]
        public void UserIsRedirectedToDefaultPage() {
            var loginPage = new TestableLogin();
            loginPage.EnteredUserName = "user";
            loginPage.EnteredPassword = "user";

            loginPage.LoginButtonClick(null, null);

            Assert.AreEqual("Default.aspx", loginPage.RedirectedToUrl);
        }
    }
}