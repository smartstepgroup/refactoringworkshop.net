#region Usings

using AwfulVideoStore;

#endregion

namespace UnitTests {
    public class TestableLogin : Login {
        public string EnteredUserName;
        public string EnteredPassword;
        public string RedirectedToUrl;
        public string SavedLogin { get; private set; }

        protected override string UserName {
            get { return EnteredUserName; }
        }

        protected override string Password {
            get { return EnteredPassword; }
        }

        protected override void SaveToSession(string login) {
            SavedLogin = login;
        }

        protected override void RedirectTo(string url) {
            RedirectedToUrl = url;
        }
    }
}