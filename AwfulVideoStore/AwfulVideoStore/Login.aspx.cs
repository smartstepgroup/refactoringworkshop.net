﻿#region Usings

using System;
using System.Web.UI;

#endregion

namespace AwfulVideoStore {
    public partial class Login : Page {
        protected void Page_Load(object sender, EventArgs e) {}

        public void LoginButtonClick(object sender, EventArgs e) {
            if (UserName == "admin" && Password == "admin") {
                SaveToSession("admin");
            }
            if (UserName == "user" && Password == "user")
            {
                SaveToSession("user");
            }

            RedirectTo("Default.aspx");
        }

        protected virtual void RedirectTo(string url)
        {
            Response.Redirect(url);
        }

        protected virtual void SaveToSession(string login)
        {
            Session["LoggedUser"] = login;
        }

        protected virtual string Password
        {
            get { return PasswordTextBox.Text; }
        }

        protected virtual string UserName
        {
            get { return UserNameTextBox.Text; }
        }
    }
}